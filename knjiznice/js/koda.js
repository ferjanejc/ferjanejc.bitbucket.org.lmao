
//google.charts.load('current', {'packages':['corechart', 'bar']});

var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var CDID = "";
var HBID = "";
var MLID = "";

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 * 
 */
 window.onload = function obNalozitvi(){
	dodaj();
}
function dodaj(){
	generirajPodatke(1);
	generirajPodatke(2);
	generirajPodatke(3);
} 
function generirajPodatke(stPacienta) {
  ehrId = "";
		 if(stPacienta == 1){
			var ime = "Chirri";
			var priimek = "Dog";
			var teza = 86;
			var visina = 186;
			var diaTlak = 80;
			var sisTlak = 100;
			var datum = "1955-05-05";
			var sessId = getSessionId();
        $.ajaxSetup({
		    headers: {"Ehr-Session": sessId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datum,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (odgovor) {
		        	console.log(teza);
		            CDID = ehrId;
		            $('#CD').append(ime+" "+priimek+" EHR ID: "+ CDID);
		            dodajMeritve(CDID, visina, teza, sisTlak, diaTlak);
		            },
		            error: function(err) { 
		            	console.log("NAPAKA!");
		            }
		        });
		    }
		});
		}
		else if(stPacienta == 2){
			var ime = "Hacienda";
			var priimek = "Boy";
			var teza = 100;
			var visina = 200;
			var diaTlak = 70;
			var sisTlak = 120;
			var datum = "1999-11-11";
			var sessId = getSessionId();
        $.ajaxSetup({
		    headers: {"Ehr-Session": sessId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datum,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (odgovor) {
		        
		            HBID = ehrId;
		            $('#HB').append(ime+" "+priimek+" EHR ID: "+ HBID);
		            dodajMeritve(HBID, visina, teza, sisTlak, diaTlak);
		            },
		            error: function(err) { 
		            	console.log("NAPAKA!");
		            }
		        });
		    }
		});
		}

		else if(stPacienta == 3){
			var ime = "Mad";
			var priimek = "Lad";
			var teza = 50;
			var visina = 168;
			var diaTlak = 75;
			var sisTlak = 115;
			var datum = "1986-08-03";
			var sessId = getSessionId();
        $.ajaxSetup({
		    headers: {"Ehr-Session": sessId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datum,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (odgovor) {
		        
		            MLID = ehrId;
		            $('#ML').append(ime+" "+priimek+" EHR ID: "+ MLID);
		            dodajMeritve(MLID, visina, teza, sisTlak, diaTlak);
		            },
		            error: function(err) { 
		            	console.log("NAPAKA!");
		            }
		        });
		    }
		});
		}

  return ehrId;
}


function uporabnik(){
    if($('#ime').val() == "" || $('#priimek').val() == ""){
        alert("IZPOLNITE VSA POLJA!");
    }
    else{
        var sessId = getSessionId();
        var ime = $('ime').val();
        var priimek = $('priimek').val();
        var datumrojstva = $('born').val();
        $.ajaxSetup({
		    headers: {"Ehr-Session": sessId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumrojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (odgovor) {
		                $('#ID').val(ehrId);
		                $("#novUporabnik").append("EHR ID: "+ ehrId);
		            },
		            error: function(err) {  // neuspešno
		            	console.log("NAPAKA!");
		            }
		        });
		    }
		});
    }
}
/*function take() {
    var sessId = getSessionId();
    $.ajaxSetup({
        headers: {"Ehr-Session": sessId}
    });
    var searchData = [
         {key: "ehrId", value: CDID}
    ];
    $.ajax({
    url: baseUrl + "/demographics/party/query",
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(searchData),
    success: function (res) {
    //  $('#teza').val();
    //  $("#header").html("Search by ehrId "+ CDID);
        for (i in res.parties) {
            var party = res.parties[i];
            $("#teza").val(party.teza);
        }
    }
});
}*/
function take() {
    var zapSt=$( "#select" ).val();
    switch(zapSt) {
        case "1":
            $("#teza").val(86);
            $("#visina").val(186);
            $("#diaTlak").val(80);
            $("#sisTlak").val(100);
            $("#ID").val(CDID);
            break;
        case "2":
            $("#teza").val(100);
            $("#visina").val(200);
            $("#diaTlak").val(70);
            $("#sisTlak").val(120);
            $("#ID").val(HBID);
            break;
        case "3":
            $("#teza").val(50);
            $("#visina").val(186);
            $("#diaTlak").val(75);
            $("#sisTlak").val(115);
            $("#ID").val(MLID);
            break;
        default:
            $("#teza").val("");
            $("#visina").val("");
            $("#diaTlak").val("");
            $("#sisTlak").val("");
            $("#ID").val("");

        }
}
function dodajMeritve(ehrId, visina, teza, sisTlak, diaTlak){
	var sessId = getSessionId();
	console.log(teza);
	$.ajaxSetup({
		    headers: {"Ehr-Session": sessId}
		});
		var podatki = {
			// Preview Structure: https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": "1957-03-10T09:08",
		    "vital_signs/height_length/any_event/body_height_length": visina,
		    "vital_signs/body_weight/any_event/body_weight": teza,
		    "vital_signs/blood_pressure/any_event/systolic": sisTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diaTlak,
		    
		};
		var parametriZahteve = {
		    "ehrId": ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: "user"
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        console.log("Vnos podatkov uspešen");
		    },
		    error: function(err) {
		    	console.log("Vnos podatkov ni bil uspešen");
		    }
		});
}
function pridobiPodatke(ehrId){
	var sessId = getSessionId();
	var ehrID = $('#ID').val();
	console.log(ehrID);
	var index = document.getElementById("select").selectedIndex;
	console.log(index);
	if(index == 0){
		alert("Izberite uporabnika!")
	}
	if(index == 4){  // to pomeni nov uporabnik
		
		var visina = $('#visina').val();
		var teza = $('#teza').val();
		var sisTlak = $('#sisTlak').val();
		var diaTlak = $('#diaTlak').val();
		var ID = $('#ID').val();
		if(ID != "" && visina != "" && teza != "" && sisTlak != "" && diaTlak != ""){ //če niso prazni 
		dodajMeritve(ID, visina, teza, sisTlak, diaTlak);
		}
		else{
			alert("izpolnite vsa polja");
		}
	}
	if(ehrID == ""){
		alert("MANJKA ID!");
	}
	$.ajax({                              //kopija
                    url: baseUrl+"/view/"+ehrID+"/"+"weight",
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessId
                    },
		success: function(teza){
			console.log(teza);
			
	$.ajax({
                    url: baseUrl+"/view/"+ehrID+"/"+"height", 
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessId
                    },	
		success: function(visina){
			console.log(teza);
			console.log(visina);
			obdelava(teza, visina);
		},
		error: function(){
					console.log("napaka");
					}
		});
					},
		error: function(){
					console.log("napaka");
					}
		});
}
function obdelava(teza,visina) {
	console.log(teza);
	console.log(visina);
	var tabelaVisine = parseInt(visina[0].height, 10);
	var tabelaTeze = parseInt(teza[0].weight, 10);
	console.log(tabelaTeze);
	console.log(tabelaVisine);
	var ITM=tabelaTeze/(tabelaVisine*tabelaVisine)*100;
	console.log(ITM);
	$('#rezultat').append("<p>Indeks telesne mase je <strong>" +ITM +"<strong></p>");
	drawChart(ITM);
    
}
function drawChart(ITM) {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Patient');
        data.addColumn('number', 'Index');
        data.addRows([
          ['pacient', ITM]/*,
          ['Onions', 1],
          ['Olives', 1],
          ['Zucchini', 1],
          ['Pepperoni', 2]*/
        ]);
        // Set chart options
        var options = {'title':'Index Telesne Mase',
                       'width':400,
                       'height':300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      
}

